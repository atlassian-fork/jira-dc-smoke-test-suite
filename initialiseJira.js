const { Builder, By, until } = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');
const { Key } = require('selenium-webdriver/lib/input')
const selectors = require('./selectors');

const { JIRA_ADMIN, JIRA_ADMIN_PWD, JIRA_BASE_URL, JIRA_LICENSE_KEY, JIRA_USER, JIRA_USER_PWD } = process.env;

const DEBUG = process.argv.includes('--debug-selenium');
if (DEBUG) console.log('In debug mode');

function createWebDriver() {
  let opts = new chrome.Options()
                       .headless()
                       .addArguments('--no-sandbox')
                       .windowSize({ height: 1920, width: 1080 });

  if (DEBUG) opts = opts.addArguments('--remote-bugging-port=9222');

  return new Builder()
    .forBrowser('chrome')
    .setChromeOptions(opts)
    .build();
}

console.log(`Configuring Jira at: ${JIRA_BASE_URL}`)

createWebDriver().then(async driver => {
  await driver.manage().setTimeouts({ implicit: 20000 });
  await driver.get(JIRA_BASE_URL);

  await tryConfigureAppProperties(driver);

  const licenseStageIdentifier = await driver.findElements(selectors.SETUP_LICENSE_INPUT_PANE);
  if (licenseStageIdentifier.length > 0) {
    await installLicense(driver);

    try {
      await driver.wait(until.elementLocated(selectors.SETUP_CREATE_ADMIN_USERNAME_INPUT), 300000, 'License did not finish loading');
    } catch(err) {
      // Sometimes the web page crashes while refreshing the license so we have to refresh and repeat license loading process
      console.log('Web page crashed, reloading jira');
      await driver.get(JIRA_BASE_URL);
  
      await tryConfigureAppProperties(driver);
      
      const retryLicenseStageIdentifier = await driver.findElements(selectors.SETUP_LICENSE_INPUT_PANE);
      if (retryLicenseStageIdentifier) {
        await installLicense(driver);
      } else {
        console.log('License already installed');
      }
    }
  } else {
    console.log('License already configured');
  }

  console.log('Trying to configure admin user');
  const adminAccountIdentifier = await driver.findElements(selectors.SETUP_CREATE_ADMIN_USERNAME_INPUT);
  if (adminAccountIdentifier.length > 0) {
    await createAdminUser(driver);
  } else {
    console.log('Admin user already set up');
  }

  console.log('Trying to configure instance notifications');
  const notificationsStageIdentifier = await driver.findElements(selectors.SETUP_NOTIFICATIONS_CONFIGURE_PANEL);
  if (notificationsStageIdentifier.length > 0) {
    console.log('Setting notification preferences');
    await driver.wait(until.elementLocated(selectors.SETUP_NOTIFICATIONS_CONFIGURE_RADIO_BUTTONS), 20000, 'Did not load email preferences page');
    const finishButton = await driver.findElement(selectors.SETUP_NEXT_BUTTON);
    await finishButton.click();
  } else {
    console.log('Already set notification preferences');
  }

  console.log('Trying to check if recovered from unfinished configure');
  const loginScreenIdentifier = await driver.findElements(selectors.DASHBOARD_PAGE);
  if (loginScreenIdentifier.length > 0) {
    console.log('Already set up instance admin. Logging in and doing admin user configuration');

    const usernameField = await driver.wait(until.elementLocated(selectors.LOGIN_FORM_USERNAME), 20000, 'Could not find username field in log in again');
    await usernameField.sendKeys(JIRA_ADMIN);

    const passwordField = await driver.findElement(selectors.DASHBOARD_LOGIN_FORM_PASSWORD);
    await passwordField.sendKeys(JIRA_ADMIN_PWD);

    const loginButton = await driver.findElement(selectors.DASHBOARD_LOGIN_BUTTON);
    await loginButton.click();
  }

  await tryChooseUserLanguage(driver);

  await tryChooseUserAvatar(driver);

  console.log('Trying to configure default project');
  const projectStageIdentifier = await driver.findElements(selectors.SETUP_TEST_PROJECT_OPTION);
  if (projectStageIdentifier.length > 0) {
    await createKanbanTestProject(driver);
  }

  console.log('Finished setting up Jira instance. Creating test user.');

  console.log('Trying to configure test user');
  await driver.get(`${JIRA_BASE_URL}/secure/admin/user/UserBrowser.jspa`);

  try{
    const confirmAdminLoginPasswordField = await driver.wait(until.elementLocated(By.id('login-form-authenticatePassword')), 5000, 'Never prompted to confirm admin credentials');
    await confirmAdminLoginPasswordField.sendKeys(JIRA_ADMIN_PWD);
    const submitConfirmationLoginButton = await driver.findElement(By.id('login-form-submit'));
    await submitConfirmationLoginButton.click();
  } catch(err) {
    console.log('Was not prompted for admin credentials. Continuing...');
  }

  const users = await driver.findElements(By.className('user-row'));
  if (users.length > 1) {
    console.log('Test user already configured')
  } else {
    await driver.get(`${JIRA_BASE_URL}/secure/admin/user/AddUser!default.jspa`);
    await createTestUser(driver);
  }
  
  console.log('Finished creating test user. Configuring test user');
  await driver.get(`${JIRA_BASE_URL}/logout`);

  const confirmLogout = await driver.findElement(By.id('confirm-logout-submit'));
  await confirmLogout.click();

  const loginAgain = await driver.wait(until.elementLocated(By.css('.aui-message a')), 20000, 'Could not find login again button');
  await loginAgain.click();

  const usernameField = await driver.wait(until.elementLocated(By.id('login-form-username')), 20000, 'Could not find username field in log in again');
  await usernameField.sendKeys(JIRA_USER);

  const passwordField = await driver.findElement(By.id('login-form-password'));
  await passwordField.sendKeys(JIRA_USER_PWD);

  const loginButton = await driver.findElement(By.id('login-form-submit'));
  await loginButton.click();

  await tryChooseUserLanguage(driver);

  await tryChooseUserAvatar(driver);

  console.log('Test user has been configured. Initialisation complete');

  driver.quit();
});

async function configureAppProperties(driver) {
  console.log('Configuring app properties');
  const submitButton = await driver.findElement(selectors.SETUP_NEXT_BUTTON);
  return submitButton.click();
}

async function tryConfigureAppProperties(driver) {
  console.log('Checking to configure app properties');
  const appPropertiesStageIdentifier = await driver.findElements(selectors.SETUP_APP_PROPERTIES_PANE);
  if (appPropertiesStageIdentifier.length > 0) {
    await configureAppProperties(driver);
  } else {
    console.log('App properties already configured');
  }
}

async function installLicense(driver) {
  console.log('Installing license');
  const licenseInputField = await driver.findElement(selectors.SETUP_LICENSE_INPUT);
  await licenseInputField.sendKeys(JIRA_LICENSE_KEY);

  const enterLicenseButton = await driver.wait(until.elementLocated(selectors.SETUP_LICENSE_CONFIRM_BUTTON));
  return enterLicenseButton.click();
}

async function createAdminUser(driver) {
  console.log('Creating admin user');
  const fullNameInput = await driver.wait(until.elementLocated(selectors.SETUP_CREATE_ADMIN_FULLNAME_INPUT), 20000, 'Did not load admin creation page');
  await fullNameInput.sendKeys('Test User');

  const emailInput = await driver.findElement(selectors.SETUP_CREATE_ADMIN_EMAIL_INPUT)
  await emailInput.sendKeys('test@test.com');

  const usernameInput = await driver.findElement(selectors.SETUP_CREATE_ADMIN_USERNAME_INPUT);
  await usernameInput.sendKeys(JIRA_ADMIN);

  const passwordInput = await driver.findElement(selectors.SETUP_CREATE_ADMIN_PASSWORD_INPUT);
  await passwordInput.sendKeys(JIRA_ADMIN_PWD);

  const confirmPasswordInput = await driver.findElement(selectors.SETUP_CREATE_ADMIN_CONFIRM_PASS_INPUT);
  await confirmPasswordInput.sendKeys(JIRA_ADMIN_PWD);

  const submitForm = await driver.findElement(selectors.SETUP_NEXT_BUTTON);
  return submitForm.click();
}

async function createKanbanTestProject(driver) {
  console.log('Creating Kanban Test project');
  const freshProjectButton = await driver.wait(until.elementLocated(selectors.SETUP_TEST_PROJECT_OPTION), 20000, 'Could not find sample project button');
  await freshProjectButton.click();

  const kanbanProject = await driver.wait(until.elementLocated(selectors.SETUP_TEST_PROJECT_KANBAN), 20000, 'Could not find kanban project option');
  await kanbanProject.click();

  const submitProjectButton = await driver.findElement(selectors.SETUP_TEST_PROJECT_CREATE_BUTTON);
  await submitProjectButton.click();

  const projectName = await driver.wait(until.elementLocated(selectors.SETUP_TEST_PROJECT_NAME_INPUT), 20000, 'Could not find project name input');
  await projectName.sendKeys('Kanban Test');

  const projectKey = await driver.wait(until.elementLocated(selectors.SETUP_TEST_PROJECT_KEY_INPUT), 20000, 'Could not find project name input');
  // Need to backspace twice because sometimes Jira will autocomplete the Project Key but sometimes it won't
  await projectKey.sendKeys(Key.BACK_SPACE);
  await projectKey.sendKeys(Key.BACK_SPACE);
  await projectKey.sendKeys('KT');

  const submitProjectNameButton = await driver.findElement(selectors.SETUP_TEST_PROJECT_CONFIRM_CREATE_BUTTON);
  await submitProjectNameButton.click();

  return driver.wait(until.elementLocated(selectors.KANBAN_BOARD_HEADER), 35000, 'Did not load new board');
}

async function createTestUser(driver) {
  console.log('Creating test user');
  const userEmailInput = driver.wait(until.elementLocated(selectors.USER_MANAGER_CREATE_USER_EMAIL_INPUT), 10000, 'User creation form never rendered');
  await userEmailInput.sendKeys('test@test.com');

  const userFullNameInput = await driver.findElement(selectors.USER_MANAGER_CREATE_USER_FULLNAME_INPUT);
  await userFullNameInput.sendKeys('Test User');

  const newUsernameInput = await driver.findElement(selectors.USER_MANAGER_CREATE_USER_USERNAME_INPUT);
  await newUsernameInput.sendKeys(JIRA_USER);

  const newUserPasswordInput = await driver.findElement(selectors.USER_MANAGER_CREATE_USER_PASSWORD_INPUT);
  await newUserPasswordInput.sendKeys(JIRA_USER_PWD);

  const newUserSubmitButton = await driver.findElement(selectors.USER_MANAGER_CREATE_USER_SUBMIT_BUTTON);
  await newUserSubmitButton.click();

  const successFlag = await driver.findElement(selectors.SUCCESS_FLAG)
  return driver.wait(until.elementIsNotVisible(successFlag), 20000, 'flag did not disappear');
}

async function chooseLanguage(driver) {
  const newUserLanguageNext = await driver.wait(until.elementLocated(selectors.SETUP_LANGUAGE_SELECT_NEXT), 25000, 'Did not get redirected to language selection');
  return newUserLanguageNext.click();
}

async function tryChooseUserLanguage(driver) {
  console.log('Trying to choose user language');
  const languageStageIdentifier = await driver.findElements(selectors.SETUP_LANGUAGE_PANE);
  if (languageStageIdentifier.length > 0) {
    console.log('Setting language preferences');
    await chooseLanguage(driver);
  }
}

async function chooseAvatar(driver) {
  const newUserAvatarNext = await driver.wait(until.elementLocated(selectors.SETUP_CHOOSE_AVATAR_DONE), 25000, 'Did not get redirected to avatar selection');
  return newUserAvatarNext.click();
}

async function tryChooseUserAvatar(driver) {
  console.log('Trying to choose user avatar');
  const avatarStageIdentifier = await driver.findElements(selectors.SETUP_CHOOSE_AVATAR_PANE);
  if (avatarStageIdentifier.length > 0) {
    console.log('Choosing avatar');
    await chooseAvatar(driver);
  }
}