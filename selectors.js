const { By } = require('selenium-webdriver');

module.exports = {
  ALL_BOARDS_LIST_MEMBER: By.css('.boards-list a'),

  CREATE_ISSUE_SUMMARY_INPUT: By.id('summary'),
  CREATE_ISSUE_SUBMIT_BUTTON: By.id('create-issue-submit'),

  DASHBOARD_PAGE: By.id('dashboard'),
  DASHBOARD_LOGIN_FORM_USERNAME: By.id('login-form-username'),
  DASHBOARD_LOGIN_FORM_PASSWORD: By.id('login-form-password'),
  DASHBOARD_LOGIN_BUTTON: By.id('login'),

  ISSUE_SEARCH_RESULT: By.css('.issue-list li'),
  ISSUE_SEARCH_RESULT_ISSUE_KEY: By.className('issue-link-key'),
  ISSUE_SEARCH_RESULTS: By.className('simple-issue-list'),
  ISSUE_SEARCH_RESULT_WITH_ISSUE_TITLE: issueTitle => By.css(`li[title="${issueTitle}"]`),
  ISSUE_SEARCH_ALT_RESULT_WITH_ISSUE_KEY: issueKey => By.css(`tr.issuerow[data-issuekey="${issueKey}"]`),

  ISSUE_VIEW_STATUS: By.id('status-val'),
  ISSUE_VIEW_SUMMARY: By.id('summary-val'),
  ISSUE_VIEW_WORKFLOW_DROPDOWN: By.id('opsbar-transitions_more'),
  ISSUE_VIEW_WORKFLOW_DROPDOWN_IN_PROGRESS: By.id('action_id_31'),

  KANBAN_BOARD_CARD: By.className('ghx-issue'),
  KANBAN_BOARD_CARD_SUMMARY_WITH_TITLE: issueTitle => By.css(`.ghx-summary[title="${issueTitle}"]`),
  KANBAN_BOARD_HEADER: By.id('ghx-board-name'),

  NAV_CREATE_ISSUE: By.id('create_link'),

  NAV_ISSUES: By.id('find_link'),
  NAV_ISSUES_DROPDOWN: By.id('find_link-content'),
  NAV_ISSUES_DROPDOWN_ELEMENT: By.css('#issues_history_main .issue-link'),
  NAV_ISSUES_DROPDOWN_REPORTED_BY_ME: By.id('filter_lnk_reported_lnk'),

  NAV_BOARDS: By.id('greenhopper_menu'),
  NAV_BOARDS_DROPDOWN_ALL_BOARDS: By.id('ghx-manageviews-mlink'),

  NAV_QUICK_SEARCH: By.id('quickSearchInput'),
  NAV_QUICK_SEARCH_RESULT_WITH_TITLE: issueTitle => By.css(`.quick-search-result-item[original-title="${issueTitle}"]`),

  SETUP_APP_PROPERTIES_PANE: By.id('jira-setupwizard-mode-public'),

  SETUP_CHOOSE_AVATAR_PANE: By.className('onboarding-sequence-avatar-picker'),
  SETUP_CHOOSE_AVATAR_DONE: By.className('avatar-picker-done'),

  SETUP_CREATE_ADMIN_FULLNAME_INPUT: By.css('input[name="fullname"]'),
  SETUP_CREATE_ADMIN_EMAIL_INPUT: By.css('input[name="email"]'),
  SETUP_CREATE_ADMIN_USERNAME_INPUT: By.css('input[name="username"]'),
  SETUP_CREATE_ADMIN_PASSWORD_INPUT: By.css('input[name="password"]'),
  SETUP_CREATE_ADMIN_CONFIRM_PASS_INPUT: By.css('input[name="confirm"]'),

  SETUP_LANGUAGE_PANE: By.className('onboarding-sequence-choose-language'),
  SETUP_LANGUAGE_SELECT_NEXT: By.id('next'),

  SETUP_LICENSE_INPUT_PANE: By.id('license-input-container'),
  SETUP_LICENSE_INPUT: By.id('licenseKey'),
  SETUP_LICENSE_CONFIRM_BUTTON: By.css('#license-input-container .aui-button'),

  SETUP_NEXT_BUTTON: By.id('jira-setupwizard-submit'),

  SETUP_NOTIFICATIONS_CONFIGURE_PANEL: By.id('jira-setupwizard-email-notifications-disabled'),
  SETUP_NOTIFICATIONS_CONFIGURE_RADIO_BUTTONS: By.className('mail-notifications-radio-options'),

  SETUP_TEST_PROJECT_OPTION: By.id('sampleData'),
  SETUP_TEST_PROJECT_KANBAN: By.css('.template[data-name="Kanban software development"]'),
  SETUP_TEST_PROJECT_CREATE_BUTTON: By.className('create-project-dialog-create-button'),
  SETUP_TEST_PROJECT_NAME_INPUT: By.id('name'),
  SETUP_TEST_PROJECT_KEY_INPUT: By.id('key'),
  SETUP_TEST_PROJECT_CONFIRM_CREATE_BUTTON: By.className('add-project-dialog-create-button'),

  UI_MASK: By.className('mask'),

  UPM_MANAGE_APPS: By.id('upm-admin-link'),
  UPM_MANAGE_APPS_SEARCH: By.id('upm-manage-filter-box'),
  UPM_PLUGIN_FEEDBACK_DIALOG: By.id('upm-vendor-feedback-dialog-container'),
  UPM_PLUGIN_ROW: By.className('upm-plugin-row'),
  UPM_POPUP: By.id('upm-plugin-status-dialog'),
  UPM_SEARCH_BOX: By.id('upm-install-search-box'),
  UPM_SEARCH_RESULT_PORTFOLIO: By.css('.upm-plugin[data-key="com.radiantminds.roadmaps-jira"]'),
  UPM_SEARCH_RESULT_PORTFOLIO_EXPANDER: By.css('.upm-plugin[data-key="com.radiantminds.roadmaps-jira"] .expander'),
  UPM_SEARCH_RESULT_PORTFOLIO_TRIAL_BUTTON: By.css('.aui-button[data-action="TRY"]'),
  UPM_SUCCESS_DIALOGUE: By.className('aui-dialog2-header-main'),
  UPM_UNINSTALL_BUTTON: By.css('.aui-button[data-action="UNINSTALL"]'),
  UPM_UNINSTALL_DIALOG_CONFIRM: By.css('.aui-dialog2-footer-actions .confirm'),
  UPM_UNINSTALL_COMPLETE_MESSAGE: By.className('upm-message-text'),

  USER_MANAGER_CREATE_USER_EMAIL_INPUT: By.id('user-create-email'),
  USER_MANAGER_CREATE_USER_FULLNAME_INPUT: By.id('user-create-fullname'),
  USER_MANAGER_CREATE_USER_USERNAME_INPUT: By.id('user-create-username'),
  USER_MANAGER_CREATE_USER_PASSWORD_INPUT: By.id('password'),
  USER_MANAGER_CREATE_USER_SUBMIT_BUTTON: By.id('user-create-submit'),

  POPUP_SUCCESS: By.className('aui-message-success'),

  SUCCESS_FLAG: By.css('.aui-message.aui-message-success.success.closeable.shadowed.aui-will-close'),

  WEBSUDO_PASSWORD_INPUT: By.id('login-form-authenticatePassword'),
  WEBSUDO_SUBMIT_LOGIN: By.id('login-form-submit'),

}